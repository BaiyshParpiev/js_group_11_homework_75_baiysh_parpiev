import React from 'react';
import {Grid, IconButton, TextField} from "@material-ui/core";
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {useDispatch, useSelector} from "react-redux";
import {
    decodeChange,
    encodeChange,
    fetchDecode,
    fetchEncode,
    passwordChange
} from "../../store/actions/passwordActions";

const Password = () => {
    const dispatch = useDispatch();
    const encodeMessage = useSelector(state => state.encodeMessage);
    const decodeMessage = useSelector(state => state.decodeMessage);
    const password = useSelector(state => state.password);

    const inputEncode = e => {
        dispatch(encodeChange(e.target.value));
    };

    const inputDecode = e => {
        dispatch(decodeChange(e.target.value))
    }

    const inputPassword = e => {
        dispatch(passwordChange(e.target.value))
    }
    const onEncodeSubmit = () => {
        dispatch(fetchEncode(encodeMessage, password));
    }

    const deCodeSubmit = () => {
        dispatch(fetchDecode(decodeMessage, password))
    }

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Grid item xs component='form'>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Encode message"
                        value={encodeMessage}
                        name="encodeMessage"
                        onChange={inputEncode}
                    />
                </Grid>
            </Grid>
            <Grid container direction="row" spacing={2}>
                <Grid item sm={6} component='form'>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Password"
                        value={password}
                        name="password"
                        onChange={inputPassword}
                    />
                </Grid>
                <IconButton onClick={onEncodeSubmit}>
                    <ExpandMoreIcon/>
                </IconButton>
                <IconButton onClick={deCodeSubmit}>
                    <ExpandLessIcon/>
                </IconButton>
            </Grid>
            <Grid container direction="column" spacing={2}>
                <Grid item xs component='form'>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Decode message"
                        value={decodeMessage}
                        name="decodeMessage"
                        onChange={inputDecode}
                    />
                </Grid>
            </Grid>
        </>
    );
};
export default Password;