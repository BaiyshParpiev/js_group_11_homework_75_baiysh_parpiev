import axios from "axios";

export const FETCH_ENCODE_REQUEST = 'FETCH_ENCODE_REQUEST';
export const FETCH_ENCODE_SUCCESS = 'FETCH_ENCODE_SUCCESS';
export const FETCH_ENCODE_FAILURE = 'FETCH_ENCODE_FAILURE';

export const FETCH_DECODE_REQUEST = 'FETCH_DECODE_REQUEST';
export const FETCH_DECODE_SUCCESS = 'FETCH_DECODE_SUCCESS';
export const FETCH_DECODE_FAILURE = 'FETCH_DECODE_FAILURE';

export const ENCODE_CHANGE = 'ENCODE_CHANGE';
export const DECODE_CHANGE = 'DECODE_CHANGE';
export const PASSWORD_CHANGE = 'PASSWORD_CHANGE';
export const ERROR = 'ERROR';
export const error = () => ({type: ERROR})

export const encodeChange = t => ({type: ENCODE_CHANGE, payload: t});
export const decodeChange = t => ({type: DECODE_CHANGE, payload: t});
export const passwordChange = t => ({type: PASSWORD_CHANGE, payload: t})


export const fetchEncodeRequest = () => ({type: FETCH_ENCODE_REQUEST});
export const fetchEncodeSuccess = encode => ({type: FETCH_ENCODE_SUCCESS, payload: encode});
export const fetchEncodeFailure = () => ({type: FETCH_ENCODE_FAILURE});


export const fetchDecodeRequest = () => ({type: FETCH_DECODE_REQUEST});
export const fetchDecodeSuccess = decode => ({type: FETCH_DECODE_SUCCESS, payload: decode});
export const fetchDecodeFailure = () => ({type: FETCH_DECODE_FAILURE});

export const fetchEncode = (message, password)  => {
    return async dispatch => {
        if(!message || !password){
            dispatch(error());
            return;
        }
       try{
           dispatch(fetchEncodeRequest());
           const data = JSON.stringify({message: message, password: password})
           const response = await axios.post('http://localhost:8000/encode', data, {headers: {
               'Content-Type': 'application/json',
           }});
           dispatch(fetchEncodeSuccess(response.data))
       }catch(e){
           dispatch(fetchEncodeFailure());
       }
    }
};

export const fetchDecode = (message, password) => {

    return async dispatch => {
        if(!message || !password){
            dispatch(error());
            return;
        }
       try{
           dispatch(fetchDecodeRequest());
           const data = JSON.stringify({message: message, password: password})
           const response = await axios.post('http://localhost:8000/decode', data, {headers: {
                   'Content-Type': 'application/json',
               }});
           dispatch(fetchDecodeSuccess(response.data))
       }catch(e){
           dispatch(fetchDecodeFailure());
       }
    }
};