import {
    DECODE_CHANGE,
    ENCODE_CHANGE, ERROR,
    FETCH_DECODE_FAILURE,
    FETCH_DECODE_REQUEST, FETCH_DECODE_SUCCESS,
    FETCH_ENCODE_FAILURE,
    FETCH_ENCODE_REQUEST,
    FETCH_ENCODE_SUCCESS, PASSWORD_CHANGE
} from "../actions/passwordActions";

const initialState = {
    fetchLoading: false,
    encodeMessage: '',
    decodeMessage: '',
    password: '',
};

const error = (state) => {
    alert('Sorry, you have to fill text fields');
    return state;
}

const passwordReducers = (state = initialState, action) => {
    switch(action.type){
        case ERROR:
            return error(state);
        case ENCODE_CHANGE:
            return {...state, encodeMessage: action.payload};
        case DECODE_CHANGE:
            return {...state, decodeMessage: action.payload};
        case PASSWORD_CHANGE:
            return {...state, password: action.payload};
        case FETCH_ENCODE_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ENCODE_SUCCESS:
            return {...state, decodeMessage: action.payload, fetchLoading: false};
        case FETCH_ENCODE_FAILURE:
            return {...state, fetchLoading: false};
        case FETCH_DECODE_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_DECODE_SUCCESS:
            return {...state, encodeMessage: action.payload, fetchLoading: false};
        case FETCH_DECODE_FAILURE:
            return {...state, fetchLoading: false};
        default:
            return state;
    }
};

export default passwordReducers;