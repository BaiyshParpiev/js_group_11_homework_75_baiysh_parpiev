import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from "react-redux";
import App from './App';
import passwordReducers from "./store/reducers/passwordReducers";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const store = createStore(passwordReducers, composeEnhancers(applyMiddleware(thunk)));

const app = (
    <Provider store={store}>
            <App/>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));

