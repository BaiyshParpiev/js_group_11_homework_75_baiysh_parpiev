const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const Vigenere = require("caesar-salad").Vigenere;

const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = 8000;


app.get('/', (req, res) => {
  res.set({'Content-type': 'text/plain'}).send('If you want to encode or decode password than you have to write with host + "/encode/something" or "/decode/something" and send post request after this you what you write you will get');
});


app.post('/encode', (req, res) => {
  if(!req.body.message || !req.body.password){
    return res.status(404).send({error: 'Date not valid'});
  }
  const password = Vigenere.Cipher('password').crypt(req.body.message)
  res.send( password);

});
app.post('/decode', (req, res) => {
  if(!req.body.message || !req.body.password){
    return res.status(404).send({error: 'Date not valid'});
  }
  const password = Vigenere.Decipher('password').crypt(req.body.message)
  res.send(password);

});

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});